import express from 'express';
import categoryService from './../services/category-service.js';
import categoryData from './../data/category-data.js';
import { validator } from '../validators/validatorMiddleware.js';
import createCategorySchema from '../validators/schemas/createCategorySchema.js';

const categoryController = express.Router();

categoryController
  .get('/', async (req, res) => {

    const categories = await categoryService.getCategories(categoryData)();

    if (!categories.length) {
      return res.status(404).send({message: 'There are no categories, yet'});
    }

    res.status(200).send(categories);
  })
  .get('/:id/quizes', async (req, res) => {

    const { id } = req.params;
    const quizes = await categoryService.getQuizesByCategory(categoryData)(+id);

    if (!quizes.length) {
      return res.status(404).send({message: 'There are no quizes in this category'});
    }

    res.status(200).send(quizes);
  })
  .post('/', validator(createCategorySchema), async (req, res) => {
    const { name } = req.body;

    const newCategory = await categoryService.postCategory(categoryData)(name);
   
    if (newCategory.message) {
        return res.status(404).send(newCategory.message);
    }
    return res.status(201).send(newCategory);
});

export default categoryController;