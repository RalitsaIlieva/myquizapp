import express from 'express';
import usersService from '../services/users-service.js';
import { validator } from '../validators/validatorMiddleware.js';
import createUserSchema from '../validators/schemas/createUserSchema.js';
import usersData from '../data/users-data.js';
import createToken from './../auth/create-token.js';

const usersController = express.Router();

usersController
  .post('/register', validator(createUserSchema), async (req, res) => {
    const createData = req.body;

    const user = await usersService.createUser(usersData)(createData);

    if (!user) {
      return res
        .status(404)
        .send({ message: 'There is a user with such username or pass!' });
    }
    res.status(201).send(user);
  })
  .post('/login', async (req, res) => {
    const { username, password } = req.body;

    const user = await usersService.signInUser(usersData)(username, password);

    if (!user) {
      res.status(404).send({ message: 'Ivalid username/password!' });
    } else {
      const payload = {
        sub: user.id,
        username: user.username,
        is_teacher: user.is_teacher,
      };

      const token = createToken(payload);

      res.status(200).send({
        token: token,
      });
    }
  });

export default usersController;
