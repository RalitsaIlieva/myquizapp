import express from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';
import helmet from 'helmet';
import categoryController from '../src/controllers/category-controller.js';
import usersController from '../src/controllers/users-controller.js';
import passport from 'passport';

const app = express();

app.use(cors(), bodyParser.json());
app.use(passport.initialize());
app.use(helmet());
app.use('/categories', categoryController);
app.use('/users', usersController);

app.use((err, req, res, next) => {
  // logger.log(err)

  res.status(500).send({
    message:
      'An unexpected error occurred, our developers are working hard to resolve it.',
  });
});

app.all('*', (req, res) =>
  res.status(404).send({ message: 'Resource not found!' }),
);

app.listen(3000, () => console.log('Listening on port 3000'));
