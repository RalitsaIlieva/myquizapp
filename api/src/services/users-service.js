import bcrypt from 'bcrypt';

const createUser = (usersData) => async (user) => {
  const { username, password, firstname, lastname } = user;

  const existingUser = await usersData.getBy('username', username);

  if (existingUser) {
    return null;
  }

  const passwordHash = await bcrypt.hash(password, 10);
  const createdUser = await usersData.create(
    username,
    passwordHash,
    firstname,
    lastname
  );

  return createdUser;
};

const signInUser = (usersData) => async (username, password) => {
  const user = await usersData.getBy('username', username);
  const userPass = await usersData.getUserPassword(username);

  if (!user || !(await bcrypt.compare(password, userPass.password))) {
    return null;
  } else {
    return user;
  }
};

export default {
  createUser,
  signInUser,
};
