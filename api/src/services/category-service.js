const getCategories = (categoryData) => async () => {
 
    return await categoryData.getAllCategories();
};

const getQuizesByCategory = (categoryData) => async (id) => {
 
    return await categoryData.getQuizesByCategory('id', id);
};

const postCategory = (categoryData) => async (name) => {

    if (await categoryData.getBy('name', name)) {
      return { message: 'This category is already registered' };
    }

    const data = await categoryData.postNewCategory(name);
    const postedCategory = await categoryData.getBy('id', data.insertId);

    return await postedCategory;
};

export default {
    getCategories,
    postCategory,
    getQuizesByCategory,
};