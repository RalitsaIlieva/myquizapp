import pool from './pool.js';

const getAllCategories = async () => {
    const sql = `
    SELECT *
    FROM categories
    `;
  
    return await pool.query(sql);
};

const getBy = async (column, value) => {
    const sql = `
    SELECT *
    FROM categories
    WHERE name = ?
    `;
  
    const result = await pool.query(sql, [value]);
  
    return result[0];
  };

  const getQuizesByCategory = async (column, value) => {
    const sql = `
    SELECT *
    FROM quizes
    WHERE categories_id = ?
    `;
  
    const result = await pool.query(sql, [value]);
    return result;
  };
  const postNewCategory = async (name) => {
    const sql = `
    INSERT categories (name)
    VALUES ('${name}');
    `;
  
    const result = await pool.query(sql);
  
    return result;
  };
export default {
    getAllCategories,
    getBy,
    postNewCategory,
    getQuizesByCategory,
};
  