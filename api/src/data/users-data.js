import pool from './pool.js';

const getBy = async (column, value) => {
  const sql = `
        SELECT id, firstname, lastname, username, is_teacher
        FROM users 
        WHERE ${column} = ?
    `;
  const result = await pool.query(sql, [value]);

  return result[0];
};

const create = async (username, password, firstname, lastname) => {
  const sql = `
    INSERT users (firstname, lastname, username, password, is_teacher)
    VALUES ('${firstname}', '${lastname}', '${username}', '${password}', 0);
    `;
  await pool.query(sql);

  const result = await getBy('username', username);

  return result;
};

const getUserPassword = async (username) => {
  const sql = ` 
  SELECT password
  FROM users
  WHERE username = ?
  `;

  const result = await pool.query(sql, [username]);

  return result[0];
};

export default {
  getBy,
  create,
  getUserPassword,
};
