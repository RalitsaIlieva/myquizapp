const loginUserSchema = {
    username: (value) => {
      if (!value) {
        return 'Username is required';
      }
      if (typeof value !== 'string' || value.length <= 4) {
        return 'Username should be a string with length more than 4.';
      }
  
      return null;
    },
    pass: (value) => {
      if (!value) {
        return 'Password is required';
      }
      if (value.length < 6 || value.length > 20) {
        return 'Password should be a string in range [6..20]';
      }
      return null;
    },
  };
  
export default loginUserSchema;