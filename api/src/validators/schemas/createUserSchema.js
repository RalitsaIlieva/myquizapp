const createUserSchema = {
  username: (value) => {
    if (typeof value !== 'string' || value.length < 3) {
      return 'Username should be a string with length more than 4.';
    }
    return null;
  },
  firstname: (value) => {
    if (typeof value !== 'string' || value.length <= 1) {
      return 'Firstname should be a string with length more than 1.';
    }
    return null;
  },
  lastname: (value) => {
    if (typeof value !== 'string' || value.length <= 1) {
      return 'Lastname should be a string with length more than 1.';
    }
    return null;
  },
  password: (value) => {
    if (!value) {
      return 'Password is required';
    }
    if (value.length < 6 || value.length > 20) {
      return 'Password should be a string in range [6..20]';
    }
    return null;
  },
};

export default createUserSchema;
