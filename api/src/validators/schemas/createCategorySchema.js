const createCategorySchema = {
    name: (value) => {
      if (!value) {
        return 'Name is required';
    }
      if (typeof value !== 'string' || value.length <= 1) {
        return 'Name of the category should be a string with length no less than 2.';
      }
      return null;
    },
}; 
    
  export default createCategorySchema;