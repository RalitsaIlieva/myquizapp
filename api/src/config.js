export const DB_CONFIG = {
  host: 'localhost',
  port: '3306',
  user: 'root',
  password: '1234',
  database: 'mydb',
};

export const PORT = 3000;

export const PRIVATE_KEY = 'your_secret_key';

// 60 mins * 60 secs
export const TOKEN_LIFETIME = 60 * 600;

export const DEFAULT_USER_ROLE = 'Student';
